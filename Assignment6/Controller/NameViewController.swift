//
//  NameViewController.swift
//  Assignment6
//
//  Created by Kadir Kadyrov on 19.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class NameViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var setNameButton: UIButton!
    
    weak var delegate: NameAndSurnameDelegate?
    private var name: String?
    private var surname: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameTextField.text = (name != nil ? name : "")
        if let curName = name {
            setNameButton.isHidden = (curName.count == 0 ? true : false)
        } else {
            setNameButton.isHidden = true
        }
        
    }
    
    @IBAction func setNameTextFieldEditing(_ sender: UITextField) {
        setNameButton.isHidden = (sender.text?.count == 0 ? true : false)
        name = sender.text
    }
    
    @IBAction func setNameButtonTouch(_ sender: Any) {
        self.name = nameTextField.text
    }
    
    func initVC(name: String, surname: String) {
        self.name = (name == "" ? nil : name)
        self.surname = (surname == "" ? nil : surname)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SurnameViewController" {
            if let surnameVC = segue.destination as? SurnameViewController {
                surnameVC.initVC(name: name ?? "", surname: surname ?? "")
                surnameVC.delegate = self
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NameViewController : NameAndSurnameDelegate {
    func updateNameAndSurname(name: String, surname: String) {
        self.name = name
        self.surname = surname
        delegate?.updateNameAndSurname(name: name, surname: surname)
    }
}
