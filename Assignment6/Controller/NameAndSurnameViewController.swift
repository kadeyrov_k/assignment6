//
//  NameAndSurnameViewController.swift
//  Assignment6
//
//  Created by Kadir Kadyrov on 19.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class NameAndSurnameViewController: UIViewController {

    @IBOutlet weak var nameAndSurnameLable: UILabel!
    @IBOutlet weak var nameAndSurnameButton: UIButton!
    
    private var name: String?
    private var surname: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNameAndSurnameLabel()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func nameAndSurnameButtonTouch(_ sender: Any) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setNameAndSurnameLabel()
    }
    
    func setNameAndSurnameLabel() {
        var helloString = "Hello, "
        guard let currentName = name else { nameAndSurnameLable.isHidden = true; nameAndSurnameButton.setTitle("Set name and surname", for: .normal); return }
        guard let currentSurname = surname else { nameAndSurnameLable.isHidden = true; nameAndSurnameButton.setTitle("Set name and surname", for: .normal); return }
        
        nameAndSurnameLable.isHidden = false
        nameAndSurnameButton.setTitle("Change name and surname", for: .normal)
        helloString.append("\(currentName) \(currentSurname)")
        nameAndSurnameLable.text = helloString
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NameViewController" {
            if let nameViewController = segue.destination as? NameViewController {
                nameViewController.initVC(name: name ?? "", surname: surname ?? "")
                nameViewController.delegate = self
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NameAndSurnameViewController : NameAndSurnameDelegate {
    func updateNameAndSurname(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    
    
}
