//
//  SurnameViewController.swift
//  Assignment6
//
//  Created by Kadir Kadyrov on 19.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

protocol NameAndSurnameDelegate: class {
    func updateNameAndSurname(name: String, surname: String)
}

class SurnameViewController: UIViewController {
    
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var setSurnameButton: UIButton!
    private var name: String?
    private var surname: String?
    weak var delegate: NameAndSurnameDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        surnameTextField.text = (surname != nil ? surname : "")
        
        if let curSurname = surname {
            setSurnameButton.isHidden = (curSurname.count == 0 ? true : false)
        } else {
            setSurnameButton.isHidden = true
        }
        
    }
    
    @IBAction func setSurnameTextFieldEditing(_ sender: UITextField) {
        setSurnameButton.isHidden = (sender.text?.count == 0 ? true : false)
        surname = sender.text
    }
    
    func initVC(name: String, surname: String) {
        self.name = (name == "" ? nil : name)
        self.surname = (surname == "" ? nil : surname)
    }
       
    @IBAction func setSurnameTouch(_ sender: Any) {
        self.surname = surnameTextField.text
        delegate?.updateNameAndSurname(name: self.name!, surname: self.surname!)
        navigationController?.popToRootViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
